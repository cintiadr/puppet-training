package { 'vim':
  ensure => 'latest',
}

file { '/home/hello-world.txt':
  ensure  => 'file',
  content => 'Help me!',	
}