class openjdk::params {
  case $::osfamily {
    'Debian': {
      $jdk_package      = 'openjdk-8-jre'
      $jdk_requirements =  [Apt::Ppa['ppa:openjdk-r/ppa']]
    }
    'RedHat':{
      $jdk_package      = 'java-1.8.0-openjdk'
      $jdk_requirements =  []
    }
    default: {
      fail("osfamily \"${::osfamily}\" not supported")
    }
  }
}
