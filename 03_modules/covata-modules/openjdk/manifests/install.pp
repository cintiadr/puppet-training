class openjdk::install inherits openjdk {
  case $::osfamily {
    'Debian': {
      include apt
      apt::key { 'ppa:openjdk-r/ppa':
          key => 'DA1A4A13543B466853BAF164EB9B1D8886F44E2A',
      }

      apt::ppa { 'ppa:openjdk-r/ppa':
        require => Apt::Key['ppa:openjdk-r/ppa']
      }
    }
  }

  class { '::java':
    version      => 'latest',
    distribution => 'jre',
    package      => $jdk_package,
    require      => $jdk_requirements,
  }
}
