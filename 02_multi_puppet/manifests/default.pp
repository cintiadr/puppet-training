node 'web' {
	package { 'vim':
	  ensure => 'latest',
	}
}

node 'db' {
	file { '/home/hello-world.txt':
	  ensure  => 'file',
	  content => 'Help me!',	
	}
}